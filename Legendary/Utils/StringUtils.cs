﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Legendary.Core.Utils
{
   public enum StringValidation
   {
      Valid,
      EmptyOrNull,
      Invalid
   };

   /// <summary>
   ///     Utilerias para facilitar el manejo de cadenas
   /// </summary>
   /// <author>Cesar Reyes</author>
   public static class StringUtils
   {
      private static readonly Random Rnd = new Random();

      public static string SafeSubstring(this string s, int startIndex, int length)
      {
         if (s.IsEmptyOrNull())
         {
            return "";
         }
         int safeStart = startIndex < 0 ? 0 : startIndex;
         safeStart = safeStart > s.Length - 1 ? s.Length - 1 : safeStart;
         int safeLength = length < 0 ? 0 : length;
         safeLength = (safeStart + safeLength) > s.Length ? s.Length - safeStart : safeLength;
         return s.Substring(safeStart, safeLength);
      }

      public static string SafeSubstring(this string s, int startIndex)
      {
         if (s.IsEmptyOrNull())
         {
            return "";
         }
         int safeStart = startIndex < 0 ? 0 : startIndex;
         safeStart = safeStart > s.Length ? s.Length : safeStart;
         return s.Substring(safeStart);
      }

      public static string SafeTrim(this string s)
      {
         if (s == null) return "";
         return s.Trim();
      }

      public static string[] SafeSplit(this string s, char splitChar)
      {
         if (s == null) return new string[] { };
         return s.Split(splitChar);
      }

      public static int SafeIndexOf(this string s, string s2)
      {
         return s == null
             ? 0
             : s.IndexOf(s2, StringComparison.Ordinal);
      }

      public static int SafeLength(this string s)
      {
         return s == null
             ? 0
             : s.Length;
      }

      public static string SafeReplace(this string s, string oldValue, string newValue)
      {
         if (s == null) return "";
         return s.Replace(oldValue, newValue);
      }

      public static string SafeReplace(this string s, char oldChar, char newChar)
      {
         if (s == null) return "";
         return s.Replace(oldChar, newChar);
      }

      public static string SafeReplaceChars(this string s, char[] oldChars, char[] newChars)
      {
         if (s == null) return "";
         for (int i = 0; i < oldChars.Count(); i++)
            s = s.Replace(oldChars[i], newChars[i]);

         return s;
      }

      public static string SafeRemoveChars(this string s, char[] invalidChars)
      {
         if (s == null) return "";
         for (int i = 0; i < invalidChars.Count(); i++)
            s = s.Replace(invalidChars[i].ToString(), "");

         return s;
      }

      public static string GetRandom(int length)
      {
         return GetRandom(length, "abcdefghijklmnopqrstuvwxyz1234567890");
      }

      public static string GetRandom(int length, string charPool)
      {
         var rs = new StringBuilder();

         while (length > 0)
         {
            double nextDouble = Rnd.NextDouble();
            int index;
            if (nextDouble == 1.0) index = charPool.Length - 1;
            else index = (int)(nextDouble * charPool.Length);
            rs.Append(charPool[index]);
            --length;
         }

         return rs.ToString();
      }

      public static string ToAccentlessString(this string s)
      {
         return s
             .Replace('á', 'a')
             .Replace('é', 'e')
             .Replace('í', 'i')
             .Replace('ó', 'o')
             .Replace('ú', 'u')
             .Replace('Á', 'A')
             .Replace('É', 'E')
             .Replace('Í', 'I')
             .Replace('Ó', 'O')
             .Replace('Ú', 'U');
      }

      public static string FormatWith(this string format, params object[] args)
      {
         if (format == null)
            throw new ArgumentNullException("format");

         return string.Format(format, args);
      }

      /// <summary>
      ///     toma las primeras N palabras de una string
      /// </summary>
      /// <param name="input"></param>
      /// <param name="N">numero de palabras</param>
      /// <returns></returns>
      public static string FirstNWords(this string input, int N)
      {
         return input.FirstNWords(N, null);
      }

      /// <summary>
      ///     toma las primeras N palabras de una string dado un conjunto de separadores
      /// </summary>
      /// <param name="input"></param>
      /// <param name="N">numero de palabras</param>
      /// <param name="separator">array de separadores</param>
      /// <returns></returns>
      public static string FirstNWords(this string input, int N, char[] separator)
      {
         return input.FirstNWords(N, separator, null);
      }

      public static string FirstNWords(this string input, int N, char[] separator, string prefix)
      {
         if (string.IsNullOrEmpty(input)) return input;
         if (separator == null) separator = new[] { ' ' };
         string retWord = "";
         string word = "";
         var words = new List<string>();

         foreach (char c in input)
         {
            if (!separator.Any(s => c == s))
               word += c;
            else
            {
               if (word != "")
                  words.Add(word);
               words.Add(c.ToString());
               word = "";
            }
         }
         if (word != "")
            words.Add(word);

         if (words.Count(w => !w.Any(c => separator.Any(s => c == s))) <= N)
            return input;

         int cont = 0;
         foreach (string word1 in words)
         {
            retWord += word1;
            if (!word1.Any(c => separator.Any(s => c == s)))
               cont++;
            if (cont >= N)
               break;
         }

         if (!string.IsNullOrEmpty(prefix)) retWord += prefix;
         return retWord;
      }

      public static string CombineUri(params string[] parts)
      {
         return parts.Where(p => !string.IsNullOrEmpty(p)).Select(p => p.Trim('/')).ToString();
      }

      public static string CombinePath(params string[] parts)
      {
         return parts.Select(p => p.Trim('\\')).ToString();
      }

      /// <summary>
      ///     Reemplaza la ocurrencia de 2 o más espacios por un solo espacio.
      /// </summary>
      /// <param name="value"></param>
      /// <returns></returns>
      public static string RemoveMultipleSpaces(this string value)
      {
         var regex = new Regex(@"[ ]{2,}", RegexOptions.None);
         return regex.Replace(value, @" ");
      }

      public static string Truncate(this string value, int newLength)
      {
         if (value == null) return "";
         return value.Length > newLength ? value.Substring(0, newLength) : value;
      }

      public static string RemoveDiacritics(this string s)
      {
         string normalizedString = s.Normalize(NormalizationForm.FormD);
         var stringBuilder = new StringBuilder();

         for (int i = 0; i < normalizedString.Length; i++)
         {
            char c = normalizedString[i];
            if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
               stringBuilder.Append(c);
         }

         return stringBuilder.ToString();
      }

      public static string ToFirstCapitalLetter(this string source)
      {
         if (string.IsNullOrEmpty(source))
            return string.Empty;
         // convert to char array of the string
         char[] letters = source.ToCharArray();
         // upper case the first char
         letters[0] = char.ToUpper(letters[0]);
         // return the array made of the new char array
         return new string(letters);
      }

      public static string ToFirstLowerCaseLetter(this string source)
      {
         if (string.IsNullOrEmpty(source))
            return string.Empty;
         // convert to char array of the string
         char[] letters = source.ToCharArray();
         // upper case the first char
         letters[0] = char.ToLower(letters[0]);
         // return the array made of the new char array
         return new string(letters);
      }

      /// <summary>
      ///     Encode a string in base 64
      /// </summary>
      public static string Base64Encode(this string str)
      {
         var encbuff = Encoding.UTF8.GetBytes(str);
         return Convert.ToBase64String(encbuff);
      }

      /// <summary>
      ///     Decode a string encoded in base 64
      /// </summary>
      public static string Base64Decode(this string str)
      {
         var decbuff = Convert.FromBase64String(str);
         return Encoding.UTF8.GetString(decbuff);
      }

      public static bool IsValidEmail(this string value)
      {
         if (value.IsEmptyOrNull())
            return false;
         const string pattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                                + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                                + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

         var regex = new Regex(pattern, RegexOptions.IgnoreCase);
         return regex.IsMatch(value);
      }

      public static string RegEXFilter(this string value, string regExFilter) {
          if (regExFilter.IsEmptyOrNull() || value.IsEmptyOrNull())
              return value;
          
          var match = Regex.Match(value, regExFilter);
          if (match.Success)
          {
              value = match.Value;
          }
          return value;
      }
   }
}