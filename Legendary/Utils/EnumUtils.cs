﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Legendary.Core.Utils
{
   public static class EnumUtils
   {
      public static IEnumerable<T> ToList<T>(IEnumerable<T> except)
      {
         if (!typeof(T).IsEnum)
            throw new InvalidCastException("This method works only with enums");

         var enums = Enum.GetValues(typeof(T)).Cast<T>();

         return except.IsEmptyOrNull()
            ? enums
            : enums.Except(except);
      }

      public static IEnumerable<T> ToList<T>()
      {
         return ToList<T>(null);
      }

      public static IEnumerable<T> ToList<T>(T except)
      {
         return ToList<T>(new List<T> { except });
      }

      public static IEnumerable<SelectListItem> ToSelectList<T>(Func<T, string> func)
      {
         var list = ToList<T>();

         return ToSelectList(list, new List<T>(), func);
      }

      public static IEnumerable<SelectListItem> ToSelectList<T>()
      {
         var list = ToList<T>();
         return ToSelectList(list, new List<T>());
      }

      public static IEnumerable<SelectListItem> ToSelectList<T>(IEnumerable<T> except, Func<T, string> func = null)
      {
         var list = ToList<T>();
         return ToSelectList(list, except, func);
      }

      public static IEnumerable<SelectListItem> ToSelectList<T>(T except, Func<T, string> func = null)
      {
         var list = ToList<T>();
         return ToSelectList(list, except, func);
      }

      public static IEnumerable<SelectListItem> ToSelectList<T>(this IEnumerable<T> list, IEnumerable<T> except, Func<T, string> func = null)
      {
         if (!typeof(T).IsEnum)
            throw new InvalidCastException("This method works only with enums");

         var toReturn = except != null && except.Any()
            ? list.Except(except)
            : list;

         return func != null
            ? toReturn.Select(
               e =>
                  new SelectListItem
                  {
                     Text = func(e),
                     Value = Convert.ToInt32(e).ToString(CultureInfo.InvariantCulture)
                  })
            : toReturn.Select(
               e =>
                  new SelectListItem
                  {
                     Text = e.ToString(),
                     Value = Convert.ToInt32(e).ToString(CultureInfo.InvariantCulture)
                  });
      }

      public static IEnumerable<SelectListItem> ToSelectList<T>(this IEnumerable<T> list, T except, Func<T, string> func = null)
      {
         return ToSelectList(list, new List<T> { except }, func);
      }
   }
}