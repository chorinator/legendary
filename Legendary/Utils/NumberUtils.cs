﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Legendary.Core.Model;

namespace Legendary.Core.Utils
{
   public static class NumberUtils
   {      
      public static string ToCurrencyString(this decimal amount, Enums.Currency currency = Enums.Currency.MXN)
      {
         var culture = (from c in CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                        let r = new RegionInfo(c.LCID)
                        where r != null
                        && r.ISOCurrencySymbol.ToUpper() == currency.ToString().ToUpper()
                        select c).FirstOrDefault();

         if (culture == null)
            return amount.ToString("0.00");

         return string.Format(culture, "{0:C}", amount);
      }
      public static string ToCurrencyString(this int amount, Enums.Currency currency = Enums.Currency.MXN)
      {
         return ToCurrencyString((decimal)amount, currency);
      }
      public static string ToCurrencyString(this double amount, Enums.Currency currency = Enums.Currency.MXN)
      {
         return ToCurrencyString((decimal)amount, currency);
      }
   }
}
