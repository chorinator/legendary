﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Text.RegularExpressions;

namespace Legendary.Core.Utils
{
   public static class HtmlUtils
   {
      public static HtmlString EnumDropdownList(this HtmlHelper html, Type enums, string name)
      {
         if (enums == null || !enums.IsEnum)
            throw new InvalidCastException("This method works only with enums");         
         var values = Enum.GetValues(enums);         
         var list = new SelectList(values);
         return html.DropDownList(name, list);
      }
      public static HtmlString EnumDropdownList<T>(this HtmlHelper html,string name,T defaultSelected, bool disabled = false, string cssClass = "" , params T[] filterEnums)
      {
         if (!typeof(T).IsEnum)
            throw new InvalidCastException("This method works only with enums");
         
         var values = filterEnums.IsNotEmptyOrNull()? 
                              Enum.GetValues(typeof(T)).Cast<T>().Where(s=> !filterEnums.Contains(s))
                              : Enum.GetValues(typeof(T)).Cast<T>().ToList();
         var attr = new Dictionary<string,object>(){ {"class",cssClass} };
         if (disabled)
            attr["disabled"] = "disabled";
         return html.DropDownList(name, values.Select(s => new SelectListItem() { 
            Text = s.ToString(),
            Value = Convert.ToInt32(s).ToString(),
            Selected = Convert.ToInt32(s) == Convert.ToInt32(defaultSelected)
         }), null,attr);
      }

      public static HtmlString EnumDropdownList(this HtmlHelper html, Type enums)
      {
         return EnumDropdownList(html, enums, enums.Name);
      }
      public static HtmlString ButtonAutoDisabled(this HtmlHelper html, string value = "Enviar", string id = "", string @class = "btn btn-primary", string formSelector = "form")
      {
         if (id.IsEmptyOrNull())
         {
            Guid g = Guid.NewGuid();
            string guid = Convert.ToBase64String(g.ToByteArray());
            Regex rgx = new Regex("[^a-zA-Z0-9]");
            guid = rgx.Replace(guid, "");
            id = ("Btn" + guid.Substring(4));
         }
         return new HtmlString(@"<button id=""" + id + @""" onclick=""javascript:$('" + formSelector + @"').submit(); $('#" + id + @"').attr('disabled','disabled');"" type=""submit"" class=""" + @class + @""">" + value + "</button>");
      }
      public static HtmlString ActionLinkAutoDisabled(this HtmlHelper html, string linkText, string actionName, string controllerName, object routeValues, string id = "", string @class = "btn btn-primary", string formSelector = "form")
      {
         if (id.IsEmptyOrNull())
         {
            Guid g = Guid.NewGuid();
            string guid = Convert.ToBase64String(g.ToByteArray());
            Regex rgx = new Regex("[^a-zA-Z0-9]");
            guid = rgx.Replace(guid, "");
            id = ("Btn" + guid.Substring(4));
         }
         return html.ActionLink(linkText, actionName, controllerName, routeValues, new { id = id, @class = @class, onclick = (@"javascript:$('" + formSelector + @"').submit(); $('#" + id + @"').attr('disabled','disabled');") });         
      }
   }
}
