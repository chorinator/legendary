﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Legendary.Core.Utils
{
    public static class SerializerUtils
    {
        public static string SerializeToJSON(this object toSerialize)
        {
           return JsonConvert.SerializeObject(toSerialize, Formatting.None,
              new JsonSerializerSettings()
              {
                 ReferenceLoopHandling = ReferenceLoopHandling.Ignore
              });
        }

        public static T DeserializeFromJSON<T>(this string toDeserialize){
            return JsonConvert.DeserializeObject<T>(toDeserialize);
        }

        public static dynamic DeserializeFromJSON(this string toDeserialize)
        {
            return JsonConvert.DeserializeObject(toDeserialize);
        }

        public static T DeepClone<T>(this T toClone)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, toClone);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }
    }
}
