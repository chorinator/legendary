using System;
using System.Linq;
using System.Linq.Expressions;
using Legendary.Core.Utils;

namespace Legendary.Core.Model.DBRequest
{
   public class SumRequest<T> : FunctionProperty<T>
   {
      protected override object Execute(IQueryable<T> original)
      {
         return original.Sum(GetLambda<decimal>());
      }

      private Expression<Func<T, TQ>> GetLambda<TQ>()
      {
         var parameter = Expression.Parameter(typeof (T));
         var property = Expression.PropertyOrField(parameter, ColumnName);

         var lambda = Expression.Lambda<Func<T, TQ>>(property, parameter);

         return lambda;
      }
   }
}