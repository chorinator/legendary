using System;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using Legendary.Core.Utils;

namespace Legendary.Core.Model.DBRequest
{
   public class DateRangeRequest<T> : DbRequestProperty<T>
   {
      public DateTime Start { get; set; }
      public DateTime End { get; set; }
      public Enums.DateRangeSearchOption SearchOption { get; set; }
      public bool? TruncateTime { get; set; }

      protected override bool IsValid()
      {
         return SearchOption != Enums.DateRangeSearchOption.None && Start.IsValidDate() && End.IsValidDate();
      }

      protected override IQueryable<T> Execute(IQueryable<T> original, string columnName)
      {
         var parameter = Expression.Parameter(typeof(T));
         var property = Expression.PropertyOrField(parameter, columnName);
         Expression predicate;

         switch (SearchOption)
         {
            case Enums.DateRangeSearchOption.Between:
               predicate = IsBetween(property);
               break;

            case Enums.DateRangeSearchOption.Outside:
               predicate = Expression.Not(IsBetween(property));
               break;

            default:
               throw new NotImplementedException(SearchOption.ToString());
         }

         var lambda = Expression.Lambda<Func<T, bool>>(predicate, parameter);

         return original.Where(lambda);
      }

      private Expression IsBetween(MemberExpression property)
      {
         var start = TruncateTime.HasValue && TruncateTime.Value
            ? Expression.Constant(Start.Date, typeof(DateTime))
            : Expression.Constant(Start, typeof(DateTime));

         var end = TruncateTime.HasValue && TruncateTime.Value
            ? Expression.Constant(End.Date, typeof(DateTime))
            : Expression.Constant(End, typeof(DateTime));

         var greaterOrLesser = Expression.And(Expression.LessThan(start, property),
               Expression.GreaterThan(end, property));
         return greaterOrLesser;
      }

      protected override Exception ThrowWhenInvalid()
      {
         if (!Start.IsValidDate())
            throw new InvalidDataException("Start be a valid date.");

         if (!End.IsValidDate())
            throw new InvalidDataException("End be a valid date.");

         if (SearchOption == Enums.DateRangeSearchOption.None)
            throw new InvalidDataException("SearchOption cannot be None.");

         throw new NotImplementedException();
      }
   }
}