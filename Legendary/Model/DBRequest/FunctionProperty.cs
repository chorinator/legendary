using System;
using System.Linq;
using System.Linq.Expressions;

namespace Legendary.Core.Model.DBRequest
{
   public abstract class FunctionProperty<T> : DbRequestProperty
   {
      public string ColumnName { get; set; }
      public string Alias { get; set; }

      protected abstract object Execute(IQueryable<T> original);
      
      public object ExecuteRequest(IQueryable<T> original)
      {
         if (IsValid())
            return Execute(original);

         throw ThrowWhenInvalid();
      }

      protected override bool IsValid()
      {
         return !String.IsNullOrWhiteSpace(ColumnName);
      }

      protected override Exception ThrowWhenInvalid()
      {
         throw new ArgumentException("Column name can't be null or empty");
      }

      public void SetColumnName<TQ>(Expression<Func<T, TQ>> column)
      {
         var name = column.Body as MemberExpression;
         if (name == null)
            throw new InvalidCastException(typeof(TQ).ToString());

         ColumnName = name.Member.Name;
      }
   }
}