using System;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using Legendary.Core.Utils;

namespace Legendary.Core.Model.DBRequest
{
   public class OrderByRequest<T> : DbRequestProperty<T>
   {
      public Enums.OrderBy OrderByDirection { get; set; }
      public string ColumnName { get; set; }

      protected override bool IsValid()
      {
         return OrderByDirection != Enums.OrderBy.None;
      }

      protected override IQueryable<T> Execute(IQueryable<T> original, string columnName)
      {
         return original.OrderBy("{0} {1}".FormatWith(columnName, OrderByDirection));
      }

      protected override Exception ThrowWhenInvalid()
      {
         throw new InvalidDataException("OrderByDirection can't be None.");
      }

      public void SetColumnName<TQ>(Expression<Func<T, TQ>> column)
      {
         var name = column.Body as MemberExpression;
         if (name == null)
            throw new InvalidCastException(typeof(T).ToString());

         ColumnName = name.Member.Name;
      }
   }
}