﻿using System.Configuration;
using System.Drawing;
using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;

namespace Legendary.AmazonServices.Utils
{
   public static class ImageStorage
   {

      internal static string AccessKey = ConfigurationManager.AppSettings["AccessKey"];

      internal static string SecretAccessKey = ConfigurationManager.AppSettings["SecretAccessKey"];

      public static string SaveImage(RegionEndpoint endPoint, System.IO.FileStream preImage, string bucket, string folder, string imageName, string imageExtension = ".png")
      {
         IAmazonS3 client;
         var amazonKey = folder + "/" + imageName + imageExtension;
         using (client = AWSClientFactory.CreateAmazonS3Client(AccessKey, SecretAccessKey, endPoint))
         {
            var request = new PutObjectRequest
            {
               InputStream = preImage,
               BucketName = bucket,
               CannedACL = S3CannedACL.Private,
               Key = amazonKey
            };

            client.PutObject(request);
            return "https://" + bucket + ".s3.amazonaws.com/" + amazonKey;
         }
      }

      public static string GetSignedUrl(string bucket, string key, RegionEndpoint endPoint = null)
      {
         IAmazonS3 client;
         endPoint = endPoint ?? RegionEndpoint.USWest2;

         using (client = AWSClientFactory.CreateAmazonS3Client(AccessKey, SecretAccessKey, endPoint))
         {

            var expiryUrlRequest = new GetPreSignedUrlRequest() { BucketName = bucket, Key = key, Expires = System.DateTime.Now.AddMinutes(8) };
            return client.GetPreSignedURL(expiryUrlRequest);
         }
      }

       public static bool IsAlreadyUploaded(string bucket, string key, RegionEndpoint endPoint = null)
       {
           IAmazonS3 client;
           endPoint = endPoint ?? RegionEndpoint.USWest2;

           using (client = AWSClientFactory.CreateAmazonS3Client(AccessKey, SecretAccessKey,  endPoint))
           {
               S3FileInfo fileInfo = new Amazon.S3.IO.S3FileInfo(client, bucket, key);
               return fileInfo.Exists;
           }
       }

       public static void DeleteImage(string bucket, string key, RegionEndpoint endPoint = null)
       {
          IAmazonS3 client;
          endPoint = endPoint ?? RegionEndpoint.USWest2;

          DeleteObjectRequest deleteObjectRequest =
                new DeleteObjectRequest
                    {
                        BucketName = bucket,
                        Key = key
                    };

          using (client = AWSClientFactory.CreateAmazonS3Client(AccessKey, SecretAccessKey, endPoint))
          {
              var operationResult = client.DeleteObject(deleteObjectRequest);
          }
       }

       public static void CopyToAnotherAmazonFolder(string sourceBucket, string sourceKey, string destinationBucket, string destinationKey, RegionEndpoint endPoint = null)
       {
            IAmazonS3 client;
            endPoint = endPoint ?? RegionEndpoint.USWest2;

            CopyObjectRequest request = new CopyObjectRequest()
            {
                SourceBucket = sourceBucket,
                SourceKey = sourceKey,
                DestinationBucket = destinationBucket,
                DestinationKey = destinationKey
            };

            using (client = AWSClientFactory.CreateAmazonS3Client(AccessKey, SecretAccessKey, endPoint))
            {
                CopyObjectResponse operationResult = client.CopyObject(request);
            }
       }

   }
}
