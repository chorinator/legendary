﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Legendary.Web.Models;

namespace Legendary.Web.Exceptions
{
   public static class HttpStatusCodeExceptionSimpleFactory
   {
      public static HttpStatusCodeException CreateException(HttpResponseMessage response, Exception innerException)
      {
         var message = response.StatusCode.ToString();

         switch (response.StatusCode)
         {
            case HttpStatusCode.BadRequest:
               return new BadRequestException(message, innerException);

            case HttpStatusCode.Conflict:
               return new ConflictException(message, innerException);

            case HttpStatusCode.InternalServerError:
               return new InternalServerErrorException(message, innerException);

            case HttpStatusCode.NotFound:
               return new NotFoundException(message, innerException);

            case HttpStatusCode.Unauthorized:
               return new UnauthorizedException(message, innerException);

            default:
               return new UnsupportedStatusCodeException(message, innerException);
         }
      }
   }
}
