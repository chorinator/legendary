﻿using System;

namespace Legendary.Web.Exceptions
{
   public class BadRequestException : HttpStatusCodeException
   {
      public BadRequestException(string message, Exception inner)
         : base(message, inner)
      {
      }
   }
}