﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Legendary.Core.Utils;

namespace Legendary.Web.MVC.Filters
{
    public abstract class BaseUserValidatorFilter : ActionFilterAttribute
    {
        public int[] _AnyPermissions;
        public int[] _AllPermissions;

        public BaseUserValidatorFilter(params int[] permissions)
        {
            _AnyPermissions = permissions;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userId = GetUserId();
            var invalidUser = userId == 0;
            var invalidPermission = false;
            if (!invalidUser)
            {
                if (_AnyPermissions.IsNotEmptyOrNull())
                {
                    invalidPermission = true;
                    foreach (var permission in _AnyPermissions)
                    {
                        if (HasPermission(userId,permission))
                        {
                            invalidPermission = false;
                            break;
                        }
                    }
                }
                if (_AllPermissions.IsNotEmptyOrNull())
                {
                    invalidPermission = true;
                    var count = 0;
                    foreach (var permission in _AllPermissions)
                    {
                        if (HasPermission(userId, permission))
                            count++;
                    }
                    invalidPermission = count != _AllPermissions.Count();
                }
            }

            if (invalidUser)//redirigir a user
            {
                var controller = (Controllers.BaseActions)filterContext.Controller;
                filterContext.Result = controller.RedirectToAction(LoginActionName, LoginControllerName, new { absoluteUri = filterContext.HttpContext.Request.Url });
            }
            if (invalidPermission)
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = "404",
                    ViewData = filterContext.Controller.ViewData
                };
            }
        }

        public virtual string LoginActionName
        {
            get{ return "Login"; }
        }
        public virtual string LoginControllerName
        {
            get{ return "Account"; }
        }        
        
        public abstract int GetUserId();
        public abstract bool HasPermission(int userId, int permission);
    }
}
