﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Legendary.Cache;
using Legendary.Cache.Models;
using Legendary.Core.Utils;

namespace Legendary.Web.MVC.Filters
{
   /// <summary>
   /// Decorates any MVC route that needs to have client requests limited by time.
   /// </summary>
   /// <remarks>
   /// Uses the current System.Web.Caching.Cache to store each client request to the decorated route.
   /// </remarks>
   [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
   public class MvcThrottleAttribute : ActionFilterAttribute
   {
      class ThrottleData
      {
         public DateTime FirstRequest { get; set; }
         public int TotalRequests { get; set; }
      }

      /// <summary>
      /// A unique name for this Throttle.
      /// </summary>
      /// <remarks>
      /// We'll be inserting a Cache record based on this name and client IP, e.g. "Name-192.168.0.1"
      /// </remarks>
      public string Name { get; set; }

      /// <summary>
      /// The number of seconds clients have to execute <see cref="RequestsAllowed"/>
      /// </summary>
      public int Seconds { get; set; }

      /// <summary>
      /// The number of requests allowed per <see cref="Seconds"/> by the same IP
      /// </summary>
      public int RequestsAllowed { get; set; }

      /// <summary>
      /// A text message that will be sent to the client upon throttling.  You can include the token {s} to
      /// show this.Seconds and {n} to show this.RequestsAllowed in the message, e.g. "Wait {s} seconds before trying {n} times again".
      /// </summary>
      public string Message { get; set; }

      public override void OnActionExecuting(ActionExecutingContext c)
      {
         Seconds = Seconds <= 0 ? 10 : Seconds;
         RequestsAllowed = RequestsAllowed <= 0 ? 10 : RequestsAllowed;
         var key = string.Concat(Name, "-", c.HttpContext.Request.UserHostAddress);
         var cache = CacheManager.Cache;
         var request = new CacheRequest
         {
            Key = key,
            Callback = () => new ThrottleData { FirstRequest = DateTime.UtcNow, TotalRequests = 1 },
         };

         var data = cache.Get<ThrottleData>(request);
         data.TotalRequests++;
         cache.Insert(new CacheItem(key, data));

         if (data.TotalRequests <= RequestsAllowed)
            return;

         if ((DateTime.UtcNow - data.FirstRequest).TotalSeconds >= Seconds)
         {
            cache.Delete(request.Key);
            return;
         }

         if (String.IsNullOrEmpty(Message))
            Message = "You may only perform this action {n} times every {s} seconds.";

         c.Result = new ContentResult { Content = Message.Replace("{n}", RequestsAllowed.ToString(CultureInfo.InvariantCulture)).Replace("{s}", Seconds.ToString(CultureInfo.InvariantCulture)) };
         // see 503 - http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
         c.HttpContext.Response.StatusCode = (int)HttpStatusCode.ServiceUnavailable;
      }
   }
}
