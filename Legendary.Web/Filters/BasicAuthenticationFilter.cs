﻿using System;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Legendary.Core.Model;

namespace Legendary.Web.Filters
{
   [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
   public abstract class BasicAuthenticationFilter : AuthorizationFilterAttribute
   {
      private readonly BasicAuthProtocol _basicAuthProtocol;
      protected BasicAuthenticationIdentity Identity;

      protected BasicAuthenticationFilter()
      {
         _basicAuthProtocol = new BasicAuthProtocol();
      }

      public override void OnAuthorization(HttpActionContext actionContext)
      {
         Identity = _basicAuthProtocol.ParseAuthorizationHeader(actionContext, DecryptAuthHeader);
         if (Identity == null)
         {
            _basicAuthProtocol.Challenge(actionContext);
            return;
         }

         if (!OnAuthenticateUser(Identity.Name, Identity.Password, actionContext))
         {
            _basicAuthProtocol.Challenge(actionContext);
            return;
         }

         var principal = new GenericPrincipal(Identity, null);

         Thread.CurrentPrincipal = principal;

         // inside of ASP.NET this is required
         //if (HttpContext.Current != null)
         //    HttpContext.Current.User = principal;

         base.OnAuthorization(actionContext);
      }

      public abstract bool OnAuthenticateUser(string username, string password, HttpActionContext actionContext);

      protected virtual string DecryptAuthHeader(string authHeader)
      {
         try
         {
            return Encoding.Default.GetString(Convert.FromBase64String(authHeader));
         }
         catch (Exception)
         {
            return String.Empty;
         }
      }
   }
}
